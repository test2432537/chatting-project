<?php

use App\Events\sendMessage;
use App\Http\Controllers\MessagesController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/public-event', function (Request $request) {
    $message = $request->post('message');
    broadcast(new sendMessage($message ));
})->middleware('throttle:60,1'); // 60 requests/minute are allowed.

Route::get('/messages', [MessagesController::class, 'index']);
Route::post('/messages', [MessagesController::class, 'store']);
