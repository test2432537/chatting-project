<?php

namespace App\Http\Controllers;

use App\Models\Message;
use Illuminate\Http\Request;

class MessagesController extends Controller
{
    public function index()
    {
        return Message::OrderBy('created_at', 'desc')->paginate(20);
    }

    public function store(Request $request)
    {
        $message = new Message();
        $message->message = $request->input('message');
        $message->save();

        return response()->json($message, 201);
    }
}
