import './bootstrap';

import {createApp} from 'vue'

import App from './components/Chat.vue'

createApp(App).mount("#app")
